<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>HackerNews</title>
</head>
<body>
<pre>
<?php
$baseUrl = 'https://hacker-news.firebaseio.com/v0';

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "{$baseUrl}/topstories.json?print=pretty");
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$topStoriesResponse = curl_exec($curl);

$itemIds = json_decode($topStoriesResponse);
for ($i = 0; $i < 100; $i++) {
    $itemId = $itemIds[$i];
    $internalUrl = "{$baseUrl}/item/{$itemId}.json?print=pretty";
    curl_setopt($curl, CURLOPT_URL, $internalUrl);
    $itemResponse = curl_exec($curl);
    $item = json_decode($itemResponse);
    echo "STORY ({$itemId}): {$item->title}<br />";
    echo "date: " . \DateTimeImmutable::createFromFormat('U', $item->time)->format('Y-m-d H:i:s') . "<br />";
    echo "internal URL: <a href=\"{$internalUrl}\">{$internalUrl}</a><br />";
    echo "external URL: <a href=\"{$item->url}\">{$item->url}</a><br />";
    echo "<hr>";
}

curl_close($curl);
?>
</pre>
</body>
